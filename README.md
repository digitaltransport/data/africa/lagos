# Lagos (Nigeria)

## Scope of Work

This repository contains a GTFS dataset collected through a data collection exercise that was conducted by [CPCS Transcom Ltd](https://cpcs.ca/) on behalf of [Lagos Metropolitan Area Transport Authority (LAMATA)](https://lamata.lagosstate.gov.ng/), with support from [Agence Française de Développement (AFD)](https://www.afd.fr/en).

In summary, the data contains:

1.	Bus stops and attribute information (i.e., frequencies, stop times, shapes)
2.	Identified bus routes and bus service types (i.e., bus lines, calendar, and informal scheduling information)

This data is visualized and is available for downloading through: [cpcslabs.ca](https://cpcslabs.ca/)

## Authority of data production

CPCS Transcom Limited (CPCS) was engaged by the Lagos Metropolitan Area Transport Authority (LAMATA) to provide Consultancy Services for the Bus Industry Transition Program (BITP). In this project, CPCS collected data through the implementation of surveys and manual traffic counts including: Bus route identification survey, Manual classified traffic counts, Bus lines identification survey, Boarding and alighting survey, Stakeholder survey of drivers, Stakeholder survey of bus owners, Stakeholder survey of ancillary service providers, Onboard OD passenger survey, Passenger quality survey / Passenger mystery survey and intermodal hub identification survey. This data was collected to better understand the existing bus operators and their routes along eight future Quality Bus Corridors (QBCs).

## Geographic Scope

The geographic area of focus of this data is along eight proposed QBCs within the mainland of Lagos State. The majority of the corridors can be described as feeder routes that enable residents to move between communities and access intermodal hubs.

The eight QBCs are:

1.	Ojuelegba – Idi Araba – Ilasamaja
2.	Iju Ishaga-Abule Egba
3.	Iyana Iba-Igando
4.	Ketu-Alapere-Akanimodo
5.	Onipanu-Oshodi
6.	Iyana Ipaja-Ayobo
7.	Yaba-Lawanson-Cele
8.	Anthony-Oshodi

These corridors are strategically located to connect passengers with other transportation modalities, such as BRT, LRT, First-and-Last-Mile buses, and other paratransit services. The corridors vary significantly in length from 1.6 km to 14.4 km. All the corridors currently host paratransit bus services with either danfo (14 pax capacity) or korope (7 pax capacity) vehicles.

## Licensing

Data is licensed under Open Data Commons Open Database License (ODbL).

## Authorship and Acknowledgement

Data collected by CPCS Transcom Ltd., Geotrans Ltd and Lagos Metropolitan Area Transport Authority.

## Data Status

This data is up to date as of December 2023.
